//////////////////////////////////////////////////////////////////////////////
/// @file test_bt.cpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the implementation file for the test of the BT class.
//////////////////////////////////////////////////////////////////////////////

#include "test_bt.h"
#include <iostream>
#include <cmath>
#include <cstdlib>
using namespace std;

CPPUNIT_TEST_SUITE_REGISTRATION (Test_bt);
void Test_bt::test_constructor()
{
  Bst <int> a;
  CPPUNIT_ASSERT(a.empty());
  CPPUNIT_ASSERT(a.size() == 0);
}

void Test_bt::test_copy_constructor()
{
  Bst <int> a;
  
  int testArray1[12] = {50, 25, 15, 20, 17, 35, 30, 75, 60, 95, 100, 99};  
  
  for(int i = 0; i < 12; i++)
  {
    a.insert(testArray1[i]);
  }
  
  Bst <int> b(a);
  
  BT <int> :: InOrder z;
  BT <int> :: InOrder y;
  
  z = a.in_begin();
  y = b.in_begin();
  
  int testArray2[12];
  int testArray3[12];

  for (int i = 0; i < a.size(); i++)
  {
    testArray2[i] = *z;
    z++;
    
    testArray3[i] = *y;
    y++;
  }
  
  for(int i = 1; i < b.size(); i++)
  {
    CPPUNIT_ASSERT(testArray2[i-1] <= testArray2[i]);
    CPPUNIT_ASSERT(testArray3[i-1] <= testArray3[i]);
    
    CPPUNIT_ASSERT(testArray2[i-1] <= testArray3[i-1]);
    CPPUNIT_ASSERT(testArray2[i] <= testArray3[i]);
    CPPUNIT_ASSERT(a.size() == b.size());
  }
}

void Test_bt::test_assignment()
{
  Bst <int> a;
  Bst <int> b;
  
  int testArray1[12] = {50, 25, 15, 20, 17, 35, 30, 75, 60, 95, 100, 99};  
  
  for(int i = 0; i < 12; i++)
  {
    a.insert(testArray1[i]);
  }
  
  b = a;
  
  BT <int> :: InOrder z;
  BT <int> :: InOrder y;
  
  z = a.in_begin();
  y = b.in_begin();
  
  int testArray2[12];
  int testArray3[12];

  for (int i = 0; i < a.size(); i++)
  {
    testArray2[i] = *z;
    z++;
    
    testArray3[i] = *y;
    y++;
  }
  
  for(int i = 1; i < b.size(); i++)
  {
    CPPUNIT_ASSERT(testArray2[i-1] <= testArray2[i]);
    CPPUNIT_ASSERT(testArray3[i-1] <= testArray3[i]);
    
    CPPUNIT_ASSERT(testArray2[i-1] <= testArray3[i-1]);
    CPPUNIT_ASSERT(testArray2[i] <= testArray3[i]);
    CPPUNIT_ASSERT(a.size() == b.size());
  }
}

void Test_bt::test_clear()
{
  Bst <int> a;
  
  int testArray1[12] = {50, 25, 15, 20, 17, 35, 30, 75, 60, 95, 100, 99};  
  
  for(int i = 0; i < 12; i++)
  {
    a.insert(testArray1[i]);
  }
  CPPUNIT_ASSERT(a.size() == 12);
  CPPUNIT_ASSERT(!a.empty());
  
  a.clear();
  
  CPPUNIT_ASSERT(a.size() == 0);
  CPPUNIT_ASSERT(a.empty());
}

void Test_bt::test_empty()
{
  Bst <int> a;
  CPPUNIT_ASSERT(a.size() == 0);
  CPPUNIT_ASSERT(a.empty());
  
  a.insert(1);
  
  CPPUNIT_ASSERT(a.size() == 1);
  CPPUNIT_ASSERT(!a.empty());
}

void Test_bt::test_size()
{
  Bst <int> a;
  int aSize = 0;
  int testArray1[12] = {50, 25, 15, 20, 17, 35, 30, 75, 60, 95, 100, 99};  
  
  for(int i = 0; i < 12; i++)
  {
    CPPUNIT_ASSERT(a.size() == aSize);
    a.insert(testArray1[i]);
    aSize++;
  }

  for(int i = 0; i < 12; i++)
  {
    CPPUNIT_ASSERT(a.size() == aSize);
    a.remove(testArray1[i]);
    aSize--;
  }
  a.clear();
  CPPUNIT_ASSERT(a.size() == 0);
}
