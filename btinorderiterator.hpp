//////////////////////////////////////////////////////////////////////////////
/// @file btinorderiterator.hpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the in-order iterator implementation file
//////////////////////////////////////////////////////////////////////////////

#include "btinorderiterator.h"

//////////////////////////////////////////////////////////////////////////////
/// @fn BTInorderIterator ()
/// @brief In-order iterator constructor.
/// @pre In-order iterator does not exist
/// @post In-order iterator exists with iterator position not set. 
/// @param None.
/// @return None (constructor)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
BTInorderIterator<generic>::BTInorderIterator ()
{
  m_current = NULL; 
}

//////////////////////////////////////////////////////////////////////////////
/// @fn BTInorderIterator (Btn<generic> *)
/// @brief In-order iterator constructor.
/// @pre In-order iterator does not exist
/// @post In-order iterator exists with iterator position already set. 
/// @param None.
/// @return None (constructor)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
BTInorderIterator<generic>::BTInorderIterator (Btn<generic> * x) : m_current (x)
{
  m_current = x;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn generic operator* () const
/// @brief Dereference operator for In-order iterator.
/// @pre Iterator is pointing at a non-NULL node of tree.
/// @post Iterator is pointing at a non-NULL node of tree.
/// @param None.
/// @return Data pointed to by node of tree.
//////////////////////////////////////////////////////////////////////////////
template <class generic>
generic BTInorderIterator<generic>::operator* () const
{
  return *(m_current -> data);
}

//////////////////////////////////////////////////////////////////////////////
/// @fn BTInorderIterator operator++ ()
/// @brief Iterate operator for iterator. Steps one step through the tree.
/// @pre Iterator is pointing at a node in the tree (or NULL if done). 
/// @post Iterator is pointing at the next node in the tree (or NULL if done). 
/// @param None.
/// @return Itself back to the iterator. It's a state machine.
//////////////////////////////////////////////////////////////////////////////
template <class generic>
BTInorderIterator<generic> BTInorderIterator<generic>::operator++ ()
{
  //So long as we're not trying to do something stupid
  if(m_current != NULL)
  {
    //Can we go right?
    if(m_current -> r != NULL)
    {
      //Go right-leftmost
      m_current = m_current -> r;
      while(m_current -> l != NULL)
      {
        m_current = m_current -> l;
      }
    }    
    else//We need to go up OR we are at the last node
    {
      //Gotta find the last node
      Btn <generic> * temp = m_current;
      
      //"Why yes, it's a terrible algorithm. I'm glad you asked!"
      //Go to the top
      while(temp != NULL && temp -> p != NULL)
      {
        temp = temp -> p;      
      }
      //Go rightmost from root node
      while(temp != NULL && temp -> r != NULL)
      {
        temp = temp -> r;
      }//Now temp is at right-most direct child of root
      
      if(m_current == temp)//We are at the last node
      {
        //We're done, set m_current to null.
        m_current = NULL;
      }
      else//We need to go up
      {
        //We're not walking off the top of tree and we're the right child
        while(m_current -> p != NULL && m_current -> p -> r == m_current)
        {
          //Go up
          m_current = m_current -> p;
        }
        //Go up once (more)
        m_current = m_current -> p;
      }//else go up
    }//Else go up or last node
    
  //If not doing something stupid. 
  }//If m_current is null, we're going to do nothing
  
  return * this;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn BTInorderIterator operator++ (int)
/// @brief Altered version of the normal ++ operator to handle preincrements
/// @pre Iterator is pointing at a node in the tree (or NULL if done). 
/// @post Iterator is pointing at the next node in the tree (or NULL if done). 
/// @param None.
/// @return Itself back to the iterator. It's a state machine.
//////////////////////////////////////////////////////////////////////////////
template <class generic>
BTInorderIterator<generic> BTInorderIterator<generic>::operator++ (int)
{
  return ++(*this);
}

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator== (const BTInorderIterator &) const
/// @brief Tests to see if iterators are pointing at the same thing.
/// @pre Two iterators exist. 
/// @post Two iterators exist. 
/// @param Second iterator.
/// @return bool: true if iterators are pointing at the same thing, false if not
//////////////////////////////////////////////////////////////////////////////
template <class generic>
bool BTInorderIterator<generic>::operator== (const BTInorderIterator & x) const
{
  return m_current == x.m_current;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator!= (const BTInorderIterator &) const
/// @brief Tests to see if iterators are pointing at the same thing.
/// @pre Two iterators exist. 
/// @post Two iterators exist. 
/// @param Second iterator.
/// @return bool: false if iterators are pointing at the same thing, true if not
//////////////////////////////////////////////////////////////////////////////
template <class generic>
bool BTInorderIterator<generic>::operator!= (const BTInorderIterator & x) const
{
  return m_current != x.m_current;
}

