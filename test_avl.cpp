//////////////////////////////////////////////////////////////////////////////
/// @file test_avl.cpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the implementation file for the test of the avl tree
//////////////////////////////////////////////////////////////////////////////

#include "test_avl.h"
#include <iostream>
#include <cmath>
#include <cstdlib>
using namespace std;

CPPUNIT_TEST_SUITE_REGISTRATION (Test_avl);

void Test_avl::test_constructor()
{
  srand(time(NULL));
  Avl <int> a;
  CPPUNIT_ASSERT(a.size() == 0);
  CPPUNIT_ASSERT(a.empty() == true);
}

void Test_avl::test_insert_bst()
{
  //Insert at root
  Avl <int> a; 
  a.insert(42);
  CPPUNIT_ASSERT(a.size() == 1);
  CPPUNIT_ASSERT(a.search(42) == 42);
  CPPUNIT_ASSERT(!a.empty());
  
  //Insert less than
  a.insert(40);
  CPPUNIT_ASSERT(a.size() == 2);
  CPPUNIT_ASSERT(a.search(40) == 40);
  
  //Insert greater than
  a.insert(45);
  CPPUNIT_ASSERT(a.size() == 3);
  CPPUNIT_ASSERT(a.search(45) == 45);

  //Insert prexisting
  a.insert(42);
  CPPUNIT_ASSERT(a.size() == 3);//No change in size
  CPPUNIT_ASSERT(a.search(42) == 42);//Still there.

}

void Test_avl::test_insert_negneg_root()
{
  Avl <int> a;   
  //Show a negative-negative rotation
  //Inserted array
  int testArrayA1[3];
  testArrayA1[0] = 50;
  testArrayA1[1] = 25;
  testArrayA1[2] = 15;

  //PreOrder array
  int testArrayA2[3];
  testArrayA2[0] = 25;
  testArrayA2[1] = 15;
  testArrayA2[2] = 50;
  
  //InOrder array
  int testArrayA3[3];
  testArrayA3[0] = 15;
  testArrayA3[1] = 25;
  testArrayA3[2] = 50;
  
  //PostOrder array
  int testArrayA4[3];
  testArrayA4[0] = 15;
  testArrayA4[1] = 50;
  testArrayA4[2] = 25;
  
  /* Inital array
            (50)
            /
         (25)
         /
      (15)
   */
  
  /* Rotated array
        (25)
        /  \
     (15)  (50)
   */
    
  for(int i = 0; i < 3; i++)
  {
    CPPUNIT_ASSERT(a.size() == i);
    a.insert(testArrayA1[i]);
    CPPUNIT_ASSERT(a.size() == i+1);
    CPPUNIT_ASSERT(a.search(testArrayA1[i]) == testArrayA1[i]);
  }
  
  BT <int> :: PreOrder a_pre = a.pre_begin();
  BT <int> :: InOrder a_in = a.in_begin();
  BT <int> :: PostOrder a_post = a.post_begin();
  
  for(int i = 0; i < 3; i++)
  {
    CPPUNIT_ASSERT(testArrayA2[i] == *a_pre);
    CPPUNIT_ASSERT(testArrayA3[i] == *a_in);
    CPPUNIT_ASSERT(testArrayA4[i] == *a_post);
    
    a_pre++;
    a_in++;
    a_post++;
  }
}

void Test_avl::test_insert_negneg_nonroot()
{
  /* Now show NegNeg case without being root... */
  Avl <int> a;
  
  //inserted array 
  int testArrayA11[5];
  testArrayA11[0] = 1;
  testArrayA11[1] = 2;
  testArrayA11[2] = 50;
  testArrayA11[3] = 25;
  testArrayA11[4] = 15;
  
  //preorder array
  int testArrayA22[5];
  testArrayA22[0] = 2;
  testArrayA22[1] = 1;
  testArrayA22[2] = 25;
  testArrayA22[3] = 15;
  testArrayA22[4] = 50;
  
  //inorder array
  int testArrayA33[5];
  testArrayA33[0] = 1;
  testArrayA33[1] = 2;
  testArrayA33[2] = 15;
  testArrayA33[3] = 25;
  testArrayA33[4] = 50;
  
  //postorder array
  int testArrayA44[5];
  testArrayA44[0] = 1;
  testArrayA44[1] = 15;
  testArrayA44[2] = 50;
  testArrayA44[3] = 25;
  testArrayA44[4] = 2;

  for(int i = 0; i < 5; i++)
  {
    CPPUNIT_ASSERT(a.size() == i);
    a.insert(testArrayA11[i]);
    CPPUNIT_ASSERT(a.size() == i+1);
    CPPUNIT_ASSERT(a.search(testArrayA11[i]) == testArrayA11[i]);
  }
 
  /* Inital array
  
          
          (2)
          / \
        (1) (50)
            /
         (25)
         /
      (15)
   */
  
  /* Rotated array
          (2)
          / \
        (1) (25)
            /  \
         (15)  (50)
   */

  BT <int> :: PreOrder a_pre = a.pre_begin();
  BT <int> :: InOrder a_in = a.in_begin();
  BT <int> :: PostOrder a_post = a.post_begin();
  
  for(int i = 0; i < 5; i++)
  {
    CPPUNIT_ASSERT(testArrayA22[i] == *a_pre);
    CPPUNIT_ASSERT(testArrayA33[i] == *a_in);
    CPPUNIT_ASSERT(testArrayA44[i] == *a_post);
    
    a_pre++;
    a_in++;
    a_post++;
  }
}

void Test_avl::test_insert_negpos_root()
{
  /* Show a negative-positive rotation */
  //nuke the previous tree so we can use it again.
  Avl <int> a;
  //Inserted array
  int testArrayA1[3];
  testArrayA1[0] = 50;
  testArrayA1[1] = 25;
  testArrayA1[2] = 35;
  
  //PreOrder array
  int testArrayA2[3];
  testArrayA2[0] = 35;
  testArrayA2[1] = 25;
  testArrayA2[2] = 50;
  
  //InOrder array
  int testArrayA3[3];
  testArrayA3[0] = 25;
  testArrayA3[1] = 35;
  testArrayA3[2] = 50;
  
  //PostOrder array
  int testArrayA4[3];
  testArrayA4[0] = 25;
  testArrayA4[1] = 50;
  testArrayA4[2] = 35;
  
  /* Inital array
            (50)
            /
         (25)
            \
           (35)
   */
  
  /* Rotated array
        (35)
        /  \
     (25)  (50)
   */
    
  for(int i = 0; i < 3; i++)
  {
    CPPUNIT_ASSERT(a.size() == i);
    a.insert(testArrayA1[i]);
    CPPUNIT_ASSERT(a.size() == i+1);
    CPPUNIT_ASSERT(a.search(testArrayA1[i]) == testArrayA1[i]);
  }
  
  BT <int> :: PreOrder a_pre = a.pre_begin();
  BT <int> :: InOrder a_in = a.in_begin();
  BT <int> :: PostOrder a_post = a.post_begin();
  
  for(int i = 0; i < 3; i++)
  {
    CPPUNIT_ASSERT(testArrayA2[i] == *a_pre);
    CPPUNIT_ASSERT(testArrayA3[i] == *a_in);
    CPPUNIT_ASSERT(testArrayA4[i] == *a_post);
    
    a_pre++;
    a_in++;
    a_post++;
  }

}

void Test_avl::test_insert_negpos_nonroot()
{
  /* Now show NegPos case without being root... */
  Avl <int> a;
  
  //inserted array 
  int testArrayA11[5];
  testArrayA11[0] = 1;
  testArrayA11[1] = 2;
  testArrayA11[2] = 50;
  testArrayA11[3] = 25;
  testArrayA11[4] = 35;
  
  //preorder array
  int testArrayA22[5];
  testArrayA22[0] = 2;
  testArrayA22[1] = 1;
  testArrayA22[2] = 35;
  testArrayA22[3] = 25;
  testArrayA22[4] = 50;
  
  //inorder array
  int testArrayA33[5];
  testArrayA33[0] = 1;
  testArrayA33[1] = 2;
  testArrayA33[2] = 25;
  testArrayA33[3] = 35;
  testArrayA33[4] = 50;
  
  //postorder array
  int testArrayA44[5];
  testArrayA44[0] = 1;
  testArrayA44[1] = 25;
  testArrayA44[2] = 50;
  testArrayA44[3] = 35;
  testArrayA44[4] = 2;

  for(int i = 0; i < 5; i++)
  {
    CPPUNIT_ASSERT(a.size() == i);
    a.insert(testArrayA11[i]);
    CPPUNIT_ASSERT(a.size() == i+1);
    CPPUNIT_ASSERT(a.search(testArrayA11[i]) == testArrayA11[i]);
  }
 
  /* Inital array
  
          
          (2)
          / \
        (1) (50)
            /
         (25)
            \
            (35)
   */
  
  /* Rotated array
          (2)
          / \
        (1) (35)
            /  \
         (25)  (50)
   */

  BT <int> :: PreOrder a_pre = a.pre_begin();
  BT <int> :: InOrder a_in = a.in_begin();
  BT <int> :: PostOrder a_post = a.post_begin();
  
  for(int i = 0; i < 5; i++)
  {
    CPPUNIT_ASSERT(testArrayA22[i] == *a_pre);
    CPPUNIT_ASSERT(testArrayA33[i] == *a_in);
    CPPUNIT_ASSERT(testArrayA44[i] == *a_post);
    
    a_pre++;
    a_in++;
    a_post++;
  }
}
void Test_avl::test_insert_posneg_root()
{
  /* Show a positive-negative rotation */
  Avl <int> a;
  //Inserted array
  int testArrayA1[3];
  testArrayA1[0] = 35;
  testArrayA1[1] = 50;
  testArrayA1[2] = 45;
  
  //PreOrder array
  int testArrayA2[3];
  testArrayA2[0] = 45;
  testArrayA2[1] = 35;
  testArrayA2[2] = 50;
  
  //InOrder array
  int testArrayA3[3];
  testArrayA3[0] = 35;
  testArrayA3[1] = 45;
  testArrayA3[2] = 50;
  
  //PostOrder array
  int testArrayA4[3];
  testArrayA4[0] = 35;
  testArrayA4[1] = 50;
  testArrayA4[2] = 45;
  
  /* Inital array
            (35)
               \
               (50)
               /
            (45)
   */
  
  /* Rotated array
        (45)
        /  \
     (35)  (50)
   */
    
  for(int i = 0; i < 3; i++)
  {
    CPPUNIT_ASSERT(a.size() == i);
    a.insert(testArrayA1[i]);
    CPPUNIT_ASSERT(a.size() == i+1);
    CPPUNIT_ASSERT(a.search(testArrayA1[i]) == testArrayA1[i]);
  }
  
  BT <int> :: PreOrder a_pre = a.pre_begin();
  BT <int> :: InOrder a_in = a.in_begin();
  BT <int> :: PostOrder a_post = a.post_begin();
  
  for(int i = 0; i < 3; i++)
  {
    CPPUNIT_ASSERT(testArrayA2[i] == *a_pre);
    CPPUNIT_ASSERT(testArrayA3[i] == *a_in);
    CPPUNIT_ASSERT(testArrayA4[i] == *a_post);
    
    a_pre++;
    a_in++;
    a_post++;
  }
}
void Test_avl::test_insert_posneg_nonroot()
{
  /* Now show PosNeg case without being root... */
  Avl <int> a;
  
  //inserted array 
 int testArrayA11[5];
  testArrayA11[0] = 1;
  testArrayA11[1] = 2;
  testArrayA11[2] = 35;
  testArrayA11[3] = 50;
  testArrayA11[4] = 45;
  
  //preorder array
  int testArrayA22[5];
  testArrayA22[0] = 2;
  testArrayA22[1] = 1;
  testArrayA22[2] = 45;
  testArrayA22[3] = 35;
  testArrayA22[4] = 50;
  
  //inorder array
  int testArrayA33[5];
  testArrayA33[0] = 1;
  testArrayA33[1] = 2;
  testArrayA33[2] = 35;
  testArrayA33[3] = 45;
  testArrayA33[4] = 50;
  
  //postorder array
  int testArrayA44[5];
  testArrayA44[0] = 1;
  testArrayA44[1] = 35;
  testArrayA44[2] = 50;
  testArrayA44[3] = 45;
  testArrayA44[4] = 2;

  for(int i = 0; i < 5; i++)
  {
    CPPUNIT_ASSERT(a.size() == i);
    a.insert(testArrayA11[i]);
    CPPUNIT_ASSERT(a.size() == i+1);
    CPPUNIT_ASSERT(a.search(testArrayA11[i]) == testArrayA11[i]);
  }
 
  /* Inital array
  
          
          (2)
          / \
        (1) (35)
               \
               (50)
               /
            (45)
   */
  
  /* Rotated array
          (2)
          / \
        (1) (45)
            /  \
         (35)  (50)
   */

  BT <int> :: PreOrder a_pre = a.pre_begin();
  BT <int> :: InOrder a_in = a.in_begin();
  BT <int> :: PostOrder a_post = a.post_begin();
  
  for(int i = 0; i < 5; i++)
  {
    CPPUNIT_ASSERT(testArrayA22[i] == *a_pre);
    CPPUNIT_ASSERT(testArrayA33[i] == *a_in);
    CPPUNIT_ASSERT(testArrayA44[i] == *a_post);
    
    a_pre++;
    a_in++;
    a_post++;
  }
}
void Test_avl::test_insert_pospos_root()
{
  //Show a positive-positive rotation
  Avl <int> a;
  //Inserted array
  int testArrayA1[3];
  testArrayA1[0] = 15;
  testArrayA1[1] = 25;
  testArrayA1[2] = 50;

  //PreOrder array
  int testArrayA2[3];
  testArrayA2[0] = 25;
  testArrayA2[1] = 15;
  testArrayA2[2] = 50;
  
  //InOrder array
  int testArrayA3[3];
  testArrayA3[0] = 15;
  testArrayA3[1] = 25;
  testArrayA3[2] = 50;
  
  //PostOrder array
  int testArrayA4[3];
  testArrayA4[0] = 15;
  testArrayA4[1] = 50;
  testArrayA4[2] = 25;
  
  /* Inital array
  
            (15)
               \
               (25)
                  \
                  (50)
   */
  
  /* Rotated array
        (25)
        /  \
     (15)  (50)
   */
    
  for(int i = 0; i < 3; i++)
  {
    CPPUNIT_ASSERT(a.size() == i);
    a.insert(testArrayA1[i]);
    CPPUNIT_ASSERT(a.size() == i+1);
    CPPUNIT_ASSERT(a.search(testArrayA1[i]) == testArrayA1[i]);
  }
  
  BT <int> :: PreOrder a_pre = a.pre_begin();
  BT <int> :: InOrder a_in = a.in_begin();
  BT <int> :: PostOrder a_post = a.post_begin();
  
  for(int i = 0; i < 3; i++)
  {
    CPPUNIT_ASSERT(testArrayA2[i] == *a_pre);
    CPPUNIT_ASSERT(testArrayA3[i] == *a_in);
    CPPUNIT_ASSERT(testArrayA4[i] == *a_post);
    
    a_pre++;
    a_in++;
    a_post++;
  }
}
void Test_avl::test_insert_pospos_nonroot()
{
  /* Now show PosPos case without being root... */
  Avl <int> a;
  
  //inserted array
  int testArrayA11[5];
  testArrayA11[0] = 1;
  testArrayA11[1] = 2;
  testArrayA11[2] = 15;
  testArrayA11[3] = 25;
  testArrayA11[4] = 50;
  
  //preorder array
  int testArrayA22[5];
  testArrayA22[0] = 2;
  testArrayA22[1] = 1;
  testArrayA22[2] = 25;
  testArrayA22[3] = 15;
  testArrayA22[4] = 50;
  
  //inorder array
  int testArrayA33[5];
  testArrayA33[0] = 1;
  testArrayA33[1] = 2;
  testArrayA33[2] = 15;
  testArrayA33[3] = 25;
  testArrayA33[4] = 50;
  
  //postorder array
  int testArrayA44[5];
  testArrayA44[0] = 1;
  testArrayA44[1] = 15;
  testArrayA44[2] = 50;
  testArrayA44[3] = 25;
  testArrayA44[4] = 2;

  for(int i = 0; i < 5; i++)
  {
    CPPUNIT_ASSERT(a.size() == i);
    a.insert(testArrayA11[i]);
    CPPUNIT_ASSERT(a.size() == i+1);
    CPPUNIT_ASSERT(a.search(testArrayA11[i]) == testArrayA11[i]);
  }
 
  /* Inital array
          (2)
          / \
        (1) (15)
               \
               (25)
                  \
                  (50)
   */
  
  /* Rotated array
          (2)
          / \
        (1) (25)
            /  \
         (15)  (50)
   */

  BT <int> :: PreOrder a_pre = a.pre_begin();
  BT <int> :: InOrder a_in = a.in_begin();
  BT <int> :: PostOrder a_post = a.post_begin();
  
  for(int i = 0; i < 5; i++)
  {
    CPPUNIT_ASSERT(testArrayA22[i] == *a_pre);
    CPPUNIT_ASSERT(testArrayA33[i] == *a_in);
    CPPUNIT_ASSERT(testArrayA44[i] == *a_post);
    
    a_pre++;
    a_in++;
    a_post++;
  }
}

void Test_avl::test_remove_bst()
{
    
  //Test case one: no children
  Avl <int> a;
  CPPUNIT_ASSERT(a.empty());//See? It's empty
  
  a.insert(42);
  CPPUNIT_ASSERT(a.size() == 1);
  CPPUNIT_ASSERT(a.search(42) == 42);
  CPPUNIT_ASSERT(!a.empty());
  //Now I have a root element in it with no children.
  
  a.remove(42);//Now I delete it
  
  //And it's gone.
  CPPUNIT_ASSERT(a.size() == 0);
  CPPUNIT_ASSERT(a.empty());
  
  
  Avl <int> b;
  int test_b1[]={50, 75, 25, 20, 30, 60, 80}; //Three layer tree
  int test_b2[]={20, 30, 60, 80}; //The first layer
  int test_b3[]={25, 75}; //The second layer
  
  for(int i = 0; i < 7; i++)
  {
    b.insert(test_b1[i]);
  }
  
  for(int i = 0; i < 4; i++)
  {
    b.remove(test_b2[i]);
    CPPUNIT_ASSERT(b.size() == 7-1-i);
    CPPUNIT_ASSERT(b.searchptr(test_b2[i])==NULL);
  }
  
  for(int i = 0; i < 2; i++)
  {
    b.remove(test_b3[i]);
    CPPUNIT_ASSERT(b.size() == 2-i);
    CPPUNIT_ASSERT(b.searchptr(test_b3[i])==NULL);
  }
  
  b.remove(50);
  
  
  
  //Test case two: only one child
  //Test left side
  Avl <int> c;
  int test_c1[]={50, 40, 30, 20};
  
  for(int i = 0; i < 4; i++)
  {
    c.insert(test_c1[i]);
  }
  
  CPPUNIT_ASSERT(c.size() == 4);
  c.remove(40);
  CPPUNIT_ASSERT(c.searchptr(40)==NULL);
  CPPUNIT_ASSERT(c.size() == 3);
  
  c.remove(50);
  CPPUNIT_ASSERT(c.searchptr(50)==NULL);
  CPPUNIT_ASSERT(c.size() == 2);
  
  //Test right side
  Avl <int> d;
  int test_d1[]={50, 60, 70, 80};
  for(int i = 0; i < 4; i++)
  {
    d.insert(test_d1[i]);
  }
  
  CPPUNIT_ASSERT(d.size() == 4);
  d.remove(60);
  CPPUNIT_ASSERT(d.searchptr(60)==NULL);
  CPPUNIT_ASSERT(d.size() == 3);
  
  d.remove(50);
  CPPUNIT_ASSERT(d.searchptr(50)==NULL);
  CPPUNIT_ASSERT(d.size() == 2);
  
  
  //Test case four -- two children, no immediate left-right child.
  //Test root
  Avl <int> e;
  int test_e1[]={50, 75, 25, 15};
  
  for(int i = 0; i < 4; i++)
  {
    e.insert(test_e1[i]);
  }
  
  e.remove(50);
  CPPUNIT_ASSERT(e.searchptr(50)==NULL);
  CPPUNIT_ASSERT(e.size() == 3);
  
  //Test non-root
  Avl <int> f;
  int test_f1[]={50, 25, 15, 35};
  for(int i = 0; i < 4; i++)
  {
    f.insert(test_f1[i]);
  }
  
  f.remove(25);
  CPPUNIT_ASSERT(f.searchptr(25)==NULL);
  CPPUNIT_ASSERT(f.size() == 3);
  
  
  //Test case three -- two children, needs swap from left-rightmost child.
  Avl <int> g;
  int test_g1[]={50, 25, 75, 35};
  for(int i = 0; i < 4; i++)
  {
    g.insert(test_g1[i]);
  }
  
  g.remove(50);
  CPPUNIT_ASSERT(g.searchptr(50)==NULL);
  CPPUNIT_ASSERT(g.size()==3);
  
  
  //Test case five
  Avl <int> h;
  int test_h1[]={50, 25, 75, 35, 30};
  for(int i = 0; i < 5; i++)
  {
    h.insert(test_h1[i]);
  }
  
  h.remove(50);
  CPPUNIT_ASSERT(h.searchptr(50)==NULL);
  CPPUNIT_ASSERT(h.size()==4);
  
  //Demonstrate handling of remove case when item to be removed does not exist.
  h.remove(50);
  CPPUNIT_ASSERT(h.size()==4);//No change in size
}

void Test_avl::test_remove_negneg_root()
{
  //remove-based Neg-Neg rotations
  //at root
  Avl <int> a;
  
  int testArrayA1[4];
  testArrayA1[0] = 50;
  testArrayA1[1] = 25;
  testArrayA1[2] = 90;
  testArrayA1[3] = 10;

  //PreOrder array
  int testArrayA2[3];
  testArrayA2[0] = 25;
  testArrayA2[1] = 10;
  testArrayA2[2] = 50;
  
  //InOrder array
  int testArrayA3[3];
  testArrayA3[0] = 10;
  testArrayA3[1] = 25;
  testArrayA3[2] = 50;
  
  //PostOrder array
  int testArrayA4[3];
  testArrayA4[0] = 10;
  testArrayA4[1] = 50;
  testArrayA4[2] = 25;
  
  /* Inital array
            (50)
            /  \
         (25)  (90)<-- Delete
         /
      (10)
   */
  
  /* Rotated array
  
        (25)
        /  \
     (15)  (50)
   */
    
  for(int i = 0; i < 4; i++)
  {
    CPPUNIT_ASSERT(a.size() == i);
    a.insert(testArrayA1[i]);
    CPPUNIT_ASSERT(a.size() == i+1);
    CPPUNIT_ASSERT(a.search(testArrayA1[i]) == testArrayA1[i]);
  }
  
  a.remove(90);
  
  BT <int> :: PreOrder a_pre = a.pre_begin();
  BT <int> :: InOrder a_in = a.in_begin();
  BT <int> :: PostOrder a_post = a.post_begin();
  
  for(int i = 0; i < 3; i++)
  {
    CPPUNIT_ASSERT(testArrayA2[i] == *a_pre);  
    CPPUNIT_ASSERT(testArrayA3[i] == *a_in);
    CPPUNIT_ASSERT(testArrayA4[i] == *a_post);
    
    a_pre++;
    a_in++;
    a_post++;
  }
}

void Test_avl::test_remove_negneg_nonroot()
{
  /* Now show NegNeg case without being root... */
  Avl <int> a;
  
  //inserted array 
  int testArrayA11[8];
  testArrayA11[0] = 50;
  testArrayA11[1] = 45;
  testArrayA11[2] = 40;
  testArrayA11[3] = 35;
  testArrayA11[4] = 30;
  testArrayA11[5] = 25;
  testArrayA11[6] = 31;
  testArrayA11[7] = 20;
  
  //preorder array
  int testArrayA22[7];
  testArrayA22[0] = 35;
  testArrayA22[1] = 25;
  testArrayA22[2] = 20;
  testArrayA22[3] = 30;
  testArrayA22[4] = 45;
  testArrayA22[5] = 40;
  testArrayA22[6] = 50;
  
  //inorder array
  int testArrayA33[7];
  testArrayA33[0] = 20;
  testArrayA33[1] = 25;
  testArrayA33[2] = 30;
  testArrayA33[3] = 35;
  testArrayA33[4] = 40;
  testArrayA33[5] = 45;
  testArrayA33[6] = 50;
  
  //postorder array
  int testArrayA44[7];
  testArrayA44[0] = 20;
  testArrayA44[1] = 30;
  testArrayA44[2] = 25;
  testArrayA44[3] = 40;
  testArrayA44[4] = 50;
  testArrayA44[5] = 45;
  testArrayA44[6] = 35;

  for(int i = 0; i < 8; i++)
  {
    CPPUNIT_ASSERT(a.size() == i);
    a.insert(testArrayA11[i]);
    CPPUNIT_ASSERT(a.size() == i+1);
    CPPUNIT_ASSERT(a.search(testArrayA11[i]) == testArrayA11[i]);
  }

  a.remove(31);

  BT <int> :: PreOrder a_pre = a.pre_begin();
  BT <int> :: InOrder a_in = a.in_begin();
  BT <int> :: PostOrder a_post = a.post_begin();

  for(int i = 0; i < 7; i++)
  {
    CPPUNIT_ASSERT(testArrayA22[i] == *a_pre);
    CPPUNIT_ASSERT(testArrayA33[i] == *a_in);
    CPPUNIT_ASSERT(testArrayA44[i] == *a_post);
    
    a_pre++;
    a_in++;
    a_post++;
  }
}

void Test_avl::test_remove_negpos_root()
{
  Avl <int> a;
  
  //inserted array 
  int testArrayA11[4];
  testArrayA11[0] = 50;
  testArrayA11[1] = 25;
  testArrayA11[2] = 75;
  testArrayA11[3] = 35;

  
  //preorder array
  int testArrayA22[3];
  testArrayA22[0] = 35;
  testArrayA22[1] = 25;
  testArrayA22[2] = 50;
  
  //inorder array
  int testArrayA33[3];
  testArrayA33[0] = 25;
  testArrayA33[1] = 35;
  testArrayA33[2] = 50;

  //postorder array
  int testArrayA44[3];
  testArrayA44[0] = 25;
  testArrayA44[1] = 50;
  testArrayA44[2] = 35;
  
  for(int i = 0; i < 4; i++)
  {
    CPPUNIT_ASSERT(a.size() == i);
    a.insert(testArrayA11[i]);
    CPPUNIT_ASSERT(a.size() == i+1);
    CPPUNIT_ASSERT(a.search(testArrayA11[i]) == testArrayA11[i]);
  }

  a.remove(75);

  BT <int> :: PreOrder a_pre = a.pre_begin();
  BT <int> :: InOrder a_in = a.in_begin();
  BT <int> :: PostOrder a_post = a.post_begin();

  for(int i = 0; i < 3; i++)
  {
    CPPUNIT_ASSERT(testArrayA22[i] == *a_pre);
    CPPUNIT_ASSERT(testArrayA33[i] == *a_in);
    CPPUNIT_ASSERT(testArrayA44[i] == *a_post);
    
    a_pre++;
    a_in++;
    a_post++;
  }
}

void Test_avl::test_remove_negpos_nonroot()
{
   /* Now show NegPos case without being root... */
  Avl <int> a;
  
  //inserted array 
  int testArrayA11[7];
  testArrayA11[0] = 100;
  testArrayA11[1] = 50;
  testArrayA11[2] = 120;
  testArrayA11[3] = 75;
  testArrayA11[4] = 25;
  testArrayA11[5] = 35;
  testArrayA11[6] = 15;

  
  //preorder array
  int testArrayA22[5];
  testArrayA22[0] = 50;
  testArrayA22[1] = 25;
  testArrayA22[2] = 35;
  testArrayA22[3] = 100;
  testArrayA22[4] = 120;

  
  //inorder array
  int testArrayA33[5];
  testArrayA33[0] = 25;
  testArrayA33[1] = 35;
  testArrayA33[2] = 50;
  testArrayA33[3] = 100;
  testArrayA33[4] = 120;
  
  //postorder array
  int testArrayA44[5];
  testArrayA44[0] = 35;
  testArrayA44[1] = 25;
  testArrayA44[2] = 120;
  testArrayA44[3] = 100;
  testArrayA44[4] = 50;

  for(int i = 0; i < 7; i++)
  {
    CPPUNIT_ASSERT(a.size() == i);
    a.insert(testArrayA11[i]);
    CPPUNIT_ASSERT(a.size() == i+1);
    CPPUNIT_ASSERT(a.search(testArrayA11[i]) == testArrayA11[i]);
  }

  a.remove(75);
  a.remove(15);

  BT <int> :: PreOrder a_pre = a.pre_begin();
  BT <int> :: InOrder a_in = a.in_begin();
  BT <int> :: PostOrder a_post = a.post_begin();

  for(int i = 0; i < 5; i++)
  {
    CPPUNIT_ASSERT(testArrayA22[i] == *a_pre);
    CPPUNIT_ASSERT(testArrayA33[i] == *a_in);
    CPPUNIT_ASSERT(testArrayA44[i] == *a_post);
    
    a_pre++;
    a_in++;
    a_post++;
  }
}

void Test_avl::test_remove_posneg_root()
{
  Avl <int> a;
  
  //inserted array 
  int testArrayA11[4];
  testArrayA11[0] = 50;
  testArrayA11[1] = 25;
  testArrayA11[2] = 75;
  testArrayA11[3] = 65;

  
  //preorder array
  int testArrayA22[3];
  testArrayA22[0] = 65;
  testArrayA22[1] = 50;
  testArrayA22[2] = 75;
  
  //inorder array
  int testArrayA33[3];
  testArrayA33[0] = 50;
  testArrayA33[1] = 65;
  testArrayA33[2] = 75;

  //postorder array
  int testArrayA44[3];
  testArrayA44[0] = 50;
  testArrayA44[1] = 75;
  testArrayA44[2] = 65;
  
  for(int i = 0; i < 4; i++)
  {
    CPPUNIT_ASSERT(a.size() == i);
    a.insert(testArrayA11[i]);
    CPPUNIT_ASSERT(a.size() == i+1);
    CPPUNIT_ASSERT(a.search(testArrayA11[i]) == testArrayA11[i]);
  }

  a.remove(25);

  BT <int> :: PreOrder a_pre = a.pre_begin();
  BT <int> :: InOrder a_in = a.in_begin();
  BT <int> :: PostOrder a_post = a.post_begin();

  for(int i = 0; i < 3; i++)
  {
    CPPUNIT_ASSERT(testArrayA22[i] == *a_pre);
    CPPUNIT_ASSERT(testArrayA33[i] == *a_in);
    CPPUNIT_ASSERT(testArrayA44[i] == *a_post);
    
    a_pre++;
    a_in++;
    a_post++;
  }
}
