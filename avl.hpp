template <class generic>
void Avl<generic>::insert(generic x)
{
  Btn <generic> * temp = Bst<generic>::p_insert(x);
  
  //Work our way up the tree
  while(temp != NULL)
  {
    //If, at any point, we find we are unbalanced
    if(unbalanced(temp))
    {
      rotate(temp);
    }
    else
    {
      setDepth(temp);
    }
    temp = temp -> p;
  }
}

template <class generic>
void Avl<generic>::remove(generic x)
{
  Btn <generic> * temp = Bst<generic>::p_remove(x);
  
  //Work our way up the tree
  while(temp != NULL)
  {
    //If, at any point, we find we are unbalanced
    if(unbalanced(temp))
    {
      rotate(temp);
    }
    else
    {
      setDepth(temp);
    }
    temp = temp -> p;
  }
}

template <class generic>
long Avl<generic>::getDepth(Btn <generic> * x)
{
  long theDepth;
  if(x != NULL)
  {
    theDepth = x -> depth;
  }
  else
  {
    theDepth = 0;
  }
  return theDepth;
}

template <class generic>
int Avl<generic>::getBalance(Btn <generic> * x)
{
  long leftDepth;
  long rightDepth;
  long longBalance;

  if(x != NULL)
  {
    leftDepth = getDepth(x->l);
    rightDepth = getDepth(x->r);   
  }

  longBalance = rightDepth - leftDepth;

  return (int)longBalance;
}

template <class generic>
bool Avl<generic>::balanced(Btn <generic> * x)
{
  if(abs(getBalance(x)) < 2)
  {
    return true;
  }
  else
  {
    return false;
  }
}

template <class generic>
bool Avl<generic>::unbalanced(Btn <generic> * x)
{
  if(abs(getBalance(x)) > 1)
  {
    return true;
  }
  else
  {
    return false;
  }
}

template <class generic>
void Avl<generic>::setDepth(Btn <generic> * x)
{
  long theDepth;
  
  long leftDepth;
  long rightDepth;
  if(x != NULL)
  {
    leftDepth = getDepth(x -> l);
    rightDepth = getDepth(x -> r);
    if(leftDepth > rightDepth)
    {
      theDepth = leftDepth;
    }
    else
    {
      theDepth = rightDepth;
    }
    x -> depth = theDepth + 1;
  }
}

template <class generic>
void Avl<generic>::rotate(Btn <generic> * x)
{
  if(getBalance(x) < 0) // -2
  {
    if(getBalance(x -> l) < 0) // -1
    {
      std::cerr << "\n rotateNegNeg " << *(x->data);
      rotateNegNeg(x);
    }
    else // +1
    {
      std::cerr << "\n rotateNegPos " << *(x->data);
      rotateNegPos(x);
    }
  }
  else// +2
  {
    if(getBalance(x -> r) < 0) // -1
    {
      std::cerr << "\n rotatePosNeg " << *(x->data);
      rotatePosNeg(x);
    }
    else // +1
    {
      std::cerr << "\n rotatePosPos " << *(x->data);
      rotatePosPos(x);
    }
  }
}


template <class generic>
void Avl<generic>::rotateNegPos(Btn <generic> * x)
{
  /* Complementary diagram!
  
         (Xp)
          |      
      -2 (X)
         / \
    +1 (Y) (Xr)
       / \
    (Yl) (Z)
         / \
      (Zl) (Zr)
  
  */

  Btn <generic> * y = x -> l;
  Btn <generic> * z = y -> r;
  
  //Since Z will be the new top node, point it to its new parent
  z -> p = x -> p;
  if(z -> p == NULL)//Huh, we're root
  {
    Bst <generic>::m_root = z;//Point root down to Z.
  }
  else//Not root
  {
    /* Figure out which child we are */
    //If on left side of parent
    if(z -> p -> l == x)
    {
      z -> p -> l = z;
    }
    else//we're the right child of parent
    {
      z -> p -> r = z;
    }
  }  
  /* Sort out arrangements for Z's left children */
  //Point Y right from Z to Z's left children
  //Even handles the null case!
  y -> r = z -> l;
  
  //If z even had children, point them at their new parent
  if(y -> r != NULL)
  {
    y -> r -> p = y;
  }
  
  //Since we're promoting Z, Z should now be Y's parent.
  //Since we just moved Z's left children to Y, Z can now take Y as its child
  //Just be glad this isn't your family.
  z -> l = y;
  y -> p = z;
  
  //X now takes custody of Z's right children, on its left side.
  x -> l = z -> r;
  if(x -> l != NULL)
  {
    x -> l -> p = x;
  }
  
  //Z should also be X's parent
  z -> r = x;
  x -> p = z;
  
  //Finally, we should update the depths (in the right order)
  setDepth(x);
  setDepth(y);
  setDepth(z);
}

template <class generic>
void Avl<generic>::rotateNegNeg(Btn <generic> * x)
{
  /* Complementary Diagram!
  
        (Xp)
          |
      -2 (X)
         / \
    -1 (Y) (Xr)
       / \
     (Z) (Yr)
     / \
  (Zl) (Zr)
  
  */

  Btn <generic> * y = x -> l;
  Btn <generic> * z = y -> l;
  
  //We're promoting Y, so point y to its new parent.
  y -> p = x -> p;
  if(y -> p == NULL)//We're promoting to root
  {
    Bst <generic>::m_root = y;//Point root down to y.
  }
  else//We ain't at root
  {
    /* Figure out which child we are */
    //If on left side of parent
    if(y -> p -> l == x)
    {
      y -> p -> l = y;
    }
    else//we're the right child of parent
    {
      y -> p -> r = y;
    }
  }
    /* Have X take custody of Y's right child so we can demote X */
    //Point X to its new child.
    x -> l = y -> r;
    //Did Y even have right children?
    if(x -> l != NULL)
    {
      x -> l -> p = x;
    }
    
    //Point y at it's new child, x
    y -> r = x;

    //Point x at its new parent, y
    x -> p = y;
    
    //Finally, set depth for changed nodes
    setDepth(x);
    setDepth(y);
}

template <class generic>
void Avl<generic>::rotatePosNeg(Btn <generic> * x)
{
  /* Complementary Diagram!
  
        (Xp)
          |
         (X) +2
         / \
      (Xl) (Y) -1
           / \
         (Z) (Yr)
         / \
      (Zl) (Zr)
  
  */
  Btn <generic> * y = x -> r;
  Btn <generic> * z = y -> l;
  
  //We're promoting Z, so point it at its new parent
  z -> p = x -> p;
  if(z -> p == NULL)//We're root
  {
    Bst <generic>::m_root = z;//Point root down to z.
  }
  else //Not root, gotta figure out which child we are.
  {
    /* Figure out which child we are */
    //If on left side of parent
    if(z -> p -> l == x)
    {
      z -> p -> l = z;
    }
    else//we're the right child of parent
    {
      z -> p -> r = z;
    }
  }

  /* Sort out arrangements for Z's right children */
  //Point Y left from Z to Z's right children
  //Even handles the null case!
  y -> l = z -> r;
  
  //If z even had children, point them at their new parent
  if(y -> l != NULL)
  {
    y -> l -> p = y;
  }
  
  //Since we're promoting Z, Z should now be Y's parent.
  //Since we just moved Z's right children to Y, Z can now take Y as its child
  z -> r = y;
  y -> p = z;
  
  //X now takes custody of Z's left children, on its right side.
  x -> r = z -> l;
  //Did z even have a child there?
  if(x -> r != NULL)
  {
    x -> r -> p = x;
  }
  
  //Z should also be X's parent
  z -> l = x;
  x -> p = z;
  
  //Finally, we should update the depths (in the right order)
  setDepth(x);
  setDepth(y);
  setDepth(z); 
}

template <class generic>
void Avl<generic>::rotatePosPos(Btn <generic> * x)
{
  /* Complementary Diagram!
  
        (Xp)
          |
      +2 (X)
         / \
   +1 (Xl) (Y)
           / \
        (Yl) (Z)
             / \
          (Zl) (Zr)
  
  */


  Btn <generic> * y = x -> r;
  Btn <generic> * z = y -> r;
  
  //We're promoting Y, so point y to its new parent.
  y -> p = x -> p;
  if(y -> p == NULL)//We're promoting to root
  {
    Bst <generic>::m_root = y;//Point root down to y.
  }
  else//We ain't at root
  {
    /* Figure out which child we are */
    //If on left side of parent
    if(y -> p -> l == x)
    {
      y -> p -> l = y;
    }
    else//we're the right child of parent
    {
      y -> p -> r = y;
    }
  }    
    /* Have X take custody of Y's left child so we can demote X */
    //Point X to its new child.
    x -> r = y -> l;
    //Did Y even have right children?
    if(x -> r != NULL)
    {
      x -> r -> p = x;
    }
    
    //Point y at it's new child, x
    y -> l = x;
    //Point x at its new parent, y
    x -> p = y;
    
    //Finally, set depth for changed nodes
    setDepth(x);
    setDepth(y);

}
