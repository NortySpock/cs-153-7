//////////////////////////////////////////////////////////////////////////////
/// @file btpreorderiterator.hpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the post-order iterator implementation file
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPreorderIterator ()
/// @brief Pre-order iterator constructor.
/// @pre Pre-order iterator does not exist
/// @post Pre-order iterator exists with iterator position not set. 
/// @param None.
/// @return None (constructor)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
BTPreorderIterator<generic>::BTPreorderIterator ()
{
  m_current = NULL;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPreorderIterator (Btn<generic> *)
/// @brief Pre-order iterator constructor.
/// @pre Pre-order iterator does not exist
/// @post Pre-order iterator exists with iterator position already set. 
/// @param None.
/// @return None (constructor)
//////////////////////////////////////////////////////////////////////////////
template <class generic>
BTPreorderIterator<generic>::BTPreorderIterator (Btn<generic> * x) : m_current (x)
{
}

//////////////////////////////////////////////////////////////////////////////
/// @fn generic operator* () const
/// @brief Dereference operator for Pre-order iterator.
/// @pre Iterator is pointing at a non-NULL node of tree.
/// @post Iterator is pointing at a non-NULL node of tree.
/// @param None.
/// @return Data pointed to by node of tree.
//////////////////////////////////////////////////////////////////////////////
template <class generic>
generic BTPreorderIterator<generic>::operator* () const
{
  return *(m_current -> data);
}

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPreorderIterator operator++ ()
/// @brief Iterate operator for iterator. Steps one step through the tree.
/// @pre Iterator is pointing at a node in the tree (or NULL if done). 
/// @post Iterator is pointing at the next node in the tree (or NULL if done). 
/// @param None.
/// @return Itself back to the iterator. It's a state machine.
//////////////////////////////////////////////////////////////////////////////
template <class generic>
BTPreorderIterator<generic> BTPreorderIterator<generic>::operator++ ()
{
  if(m_current !=NULL)
  {
    //We can go left 
    if(m_current -> l != NULL)
    {
      //Go left
      m_current = m_current -> l;
    }
    else if(m_current -> r != NULL)
    {
      //go right
      m_current = m_current -> r;
    }
    else//Hit bottom, need to go up
    {
      /* While:
       * we can go up AND 
       * EITHER we cannot go right
       * OR we ARE the one on the right
       * Then:
       * Go up
       *
       * This terminate when either
       * Parent is null (done)
       * We can go right to a new node.
       */
      while(m_current -> p != NULL && (m_current -> p -> r == NULL || m_current -> p -> r  == m_current))
      {
        m_current = m_current -> p;
      }
      if(m_current -> p == NULL)
      {
        m_current = NULL;
      }
      else
      {
        m_current = m_current -> p -> r;
      }
    }
  }

  return * this;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn BTPreorderIterator operator++ (int)
/// @brief Altered version of the normal ++ operator to handle preincrements
/// @pre Iterator is pointing at a node in the tree (or NULL if done). 
/// @post Iterator is pointing at the next node in the tree (or NULL if done). 
/// @param None.
/// @return Itself back to the iterator. It's a state machine.
//////////////////////////////////////////////////////////////////////////////
template <class generic>
BTPreorderIterator<generic> BTPreorderIterator<generic>::operator++ (int)
{
  return ++(*this);
}

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator== (const BTPreorderIterator &) const
/// @brief Tests to see if iterators are pointing at the same thing.
/// @pre Two iterators exist. 
/// @post Two iterators exist. 
/// @param Second iterator.
/// @return bool: true if iterators are pointing at the same thing, false if not
//////////////////////////////////////////////////////////////////////////////
template <class generic>
bool BTPreorderIterator<generic>::operator== (const BTPreorderIterator & x) const
{
  return m_current == x.m_current;
}

//////////////////////////////////////////////////////////////////////////////
/// @fn bool operator!= (const BTPreorderIterator &) const
/// @brief Tests to see if iterators are pointing at the same thing.
/// @pre Two iterators exist. 
/// @post Two iterators exist. 
/// @param Second iterator.
/// @return bool: false if iterators are pointing at the same thing, true if not
//////////////////////////////////////////////////////////////////////////////
template <class generic>
bool BTPreorderIterator<generic>::operator!= (const BTPreorderIterator & x) const
{
  return m_current != x.m_current;
}

