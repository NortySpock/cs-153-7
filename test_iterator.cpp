//////////////////////////////////////////////////////////////////////////////
/// @file test_iterator.cpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the implementation file for the test of the iterators
//////////////////////////////////////////////////////////////////////////////

#include "test_iterator.h"
#include <iostream>
#include <cmath>
#include <cstdlib>
using namespace std;

CPPUNIT_TEST_SUITE_REGISTRATION (Test_iterator);

void Test_iterator::test_PreOrder_constructor()
{
  Bst <int> a;
  
  BT <int> :: PreOrder z;
  
  //Attach iterator to empty tree.
  try
  {  
    z = a.pre_begin();
    CPPUNIT_ASSERT(false);
  }
  catch(Exception &e)
  {
    CPPUNIT_ASSERT(e.error_code() == CONTAINER_EMPTY);
  }

  Bst <int> b;
  
  BT <int> :: PreOrder y;
  
  b.insert(50);
  
  y = b.pre_begin();
  CPPUNIT_ASSERT(*y == 50); //see? It worked!
}

void Test_iterator::test_PreOrder_copy_constructor()
{
  Bst <int> a;
  BT <int> :: PreOrder z;
  
  a.insert(50);
  z = a.pre_begin();
  CPPUNIT_ASSERT(*z == 50);
  
  BT <int> :: PreOrder y = z;
  
  CPPUNIT_ASSERT(*y == 50); //see? It worked!
}

void Test_iterator::test_PreOrder_dereference()
{
  Bst <int> b;
  BT <int> :: PreOrder y;
  
  b.insert(50);
  
  y = b.pre_begin();
  CPPUNIT_ASSERT(*y == 50);
}

void Test_iterator::test_PreOrder_iterator()
{
  int testArray1[12] = {50, 25, 15, 20, 17, 35, 30, 75, 60, 95, 100, 99};  
  
  Bst <int> a;
  BT <int> :: PreOrder z;
  
  for(int i = 0; i < 12; i++)
  {
    a.insert(testArray1[i]);
  }
  
  z = a.pre_begin();
  for(int i = 0; i < 12; i++)
  {
    CPPUNIT_ASSERT(*z == testArray1[i]);
    z++; 
  }
}

void Test_iterator::test_PreOrder_equal()
{
  Bst <int> a;
  
  BT <int> :: PreOrder y;
  BT <int> :: PreOrder z;
  
  a.insert(50);
  a.insert(25);
  
  y = a.pre_begin();
  z = a.pre_begin();
  
  CPPUNIT_ASSERT(y == z);
  z++;
  CPPUNIT_ASSERT(!(y == z));
}

void Test_iterator::test_PreOrder_not_equal()
{
  Bst <int> a;
  
  BT <int> :: PreOrder y;
  BT <int> :: PreOrder z;
  
  a.insert(50);
  a.insert(25);
  
  y = a.pre_begin();
  z = a.pre_begin();
  
  CPPUNIT_ASSERT(!(y != z));
  z++;
  CPPUNIT_ASSERT(y != z);
}

void Test_iterator::test_PreOrder_search()
{
  int testArray1[12] = {50, 25, 15, 20, 17, 35, 30, 75, 60, 95, 100, 99};  
  
  Bst <int> a;
  BT <int> :: PreOrder z;

  try
  {
    z = a.pre_search(6);//Tree does not exist, throw exception
    CPPUNIT_ASSERT(false);
  }
  catch(Exception &e)
  {
    CPPUNIT_ASSERT(e.error_code() == CONTAINER_EMPTY);
  }
  
  for(int i = 0; i < 12; i++)
  {
    a.insert(testArray1[i]);
  }
  
  z = a.pre_search(-1);//not in tree
  CPPUNIT_ASSERT(z == NULL);
  
  z = a.pre_search(17);//exists
  CPPUNIT_ASSERT(*z == 17);
}

void Test_iterator::test_InOrder_constructor()
{
  Bst <int> a;
  
  BT <int> :: InOrder z;
  
  //Attach iterator to empty tree.
  try
  {  
    z=a.in_begin();
    CPPUNIT_ASSERT(false);
  }
  catch(Exception &e)
  {
    CPPUNIT_ASSERT(e.error_code() == CONTAINER_EMPTY);
  }

  Bst <int> b;
  
  BT <int> :: InOrder y;
  
  b.insert(50);
  
  y=b.in_begin();
  CPPUNIT_ASSERT(*y == 50);
}

void Test_iterator::test_InOrder_copy_constructor()
{
  Bst <int> a;
  BT <int> :: InOrder z;
  
  a.insert(50);
  z = a.in_begin();
  CPPUNIT_ASSERT(*z == 50);
  
  BT <int> :: InOrder y = z;
  
  CPPUNIT_ASSERT(*y == 50); //see? It worked!
}

void Test_iterator::test_InOrder_dereference()
{
  Bst <int> b;
  
  BT <int> :: InOrder y;
  
  b.insert(50);
  
  y=b.in_begin();
  CPPUNIT_ASSERT(*y == 50);
}

//This actually tests all the possible cases of the iterator.
void Test_iterator::test_InOrder_iterator()
{
  Bst <int> a;
  
  BT <int> :: InOrder z;
  
  a.insert(50);
  a.insert(25);
  a.insert(75);
  a.insert(15);
  a.insert(35);
  a.insert(55);
  a.insert(95);
  
  z=a.in_begin();
  
  int testArray1[a.size()];
  
  for (int i = 0; i < a.size(); i++)
  {
    testArray1[i] = *z;
    z++;
  }
  
  for (int i = 1; i < a.size(); i++)
  {
    CPPUNIT_ASSERT(testArray1[i-1] <= testArray1[i]);
  } 
}

void Test_iterator::test_InOrder_equal()
{
  Bst <int> a;
  
  BT <int> :: InOrder y;
  BT <int> :: InOrder z;
  
  a.insert(50);
  a.insert(25);
  
  y = a.in_begin();
  z = a.in_begin();
  
  CPPUNIT_ASSERT(y == z);
  z++;
  CPPUNIT_ASSERT(!(y == z));
}

void Test_iterator::test_InOrder_not_equal()
{
  Bst <int> a;
  
  BT <int> :: InOrder y;
  BT <int> :: InOrder z;
  
  a.insert(50);
  a.insert(25);
  
  y = a.in_begin();
  z = a.in_begin();
  
  CPPUNIT_ASSERT(!(y != z));
  z++;
  CPPUNIT_ASSERT(y != z);
}

//Can I make it iterate no matter what?
void Test_iterator::test_InOrder_hammer()
{
  const int TEST_NODES = 500; //nodes in the tree
  const int TEST_ITER = 100; //number of times to run the test

  Bst <int> a;
  BT <int> :: InOrder z;

  for(int i = 0; i < TEST_ITER; i++)
  {
    srand(i); //seed with a known value (So we can reproduce it)
    
    int testArray1[TEST_NODES];
    
    for(int j = 0; j < TEST_NODES; j++)
    {
      testArray1[j] = j;
    }
    
    int arraySwap;
    int k;
    //Knuth shuffle
    for(int j = 0; j < TEST_NODES; j++)
    {
      k = rand() % TEST_NODES;
      arraySwap = testArray1[j];
      testArray1[j] = testArray1[k];
      testArray1[k] = arraySwap;    
    }

    //Use the shuffled array to populate the tree
    for(int j = 0; j < TEST_NODES; j++)
    {
      a.insert(testArray1[j]);
    }

    //Now that the tree is populated, we attach the iterator to the tree.
    z = a.in_begin();
    
    //Create an array to fill out
    int testArray2[TEST_NODES];
    
    //Populate the array with sorted stuff
    for(int j = 0; j < TEST_NODES; j++)
    {
      testArray2[j] = *z;//Output sorted list into array
      z++;
    }

    //Test that it's sorted
    for(int j = 1; j < TEST_NODES; j++)
    {
      CPPUNIT_ASSERT(testArray2[j-1] <= testArray2[j]);
    }
    a.clear();
  }//For n iterations of test
}

void Test_iterator::test_InOrder_search()
{
  int testArray1[12] = {50, 25, 15, 20, 17, 35, 30, 75, 60, 95, 100, 99};  
  
  Bst <int> a;
  BT <int> :: InOrder z;

  try
  {
    z = a.in_search(6);//Tree does not exist, throw exception
    CPPUNIT_ASSERT(false);
  }
  catch(Exception &e)
  {
    CPPUNIT_ASSERT(e.error_code() == CONTAINER_EMPTY);
  }
  
  for(int i = 0; i < 12; i++)
  {
    a.insert(testArray1[i]);
  }
  
  z = a.in_search(-1);//not in tree
  CPPUNIT_ASSERT(z == NULL);
  
  z = a.in_search(17);//exists
  CPPUNIT_ASSERT(*z == 17);
}

void Test_iterator::test_PostOrder_constructor()
{
  Bst <int> a;
  
  BT <int> :: PostOrder z;
  
  //Attach iterator to empty tree.
  try
  {  
    z = a.post_begin();
    CPPUNIT_ASSERT(false);
  }
  catch(Exception &e)
  {
    CPPUNIT_ASSERT(e.error_code() == CONTAINER_EMPTY);
  }

  Bst <int> b;
  
  BT <int> :: PostOrder y;
  
  b.insert(50);
  
  y = b.post_begin();
  CPPUNIT_ASSERT(*y == 50);
}

void Test_iterator::test_PostOrder_copy_constructor()
{
  Bst <int> a;
  BT <int> :: PostOrder z;
  
  a.insert(50);
  z = a.post_begin();
  CPPUNIT_ASSERT(*z == 50);
  
  BT <int> :: PostOrder y = z;
  
  CPPUNIT_ASSERT(*y == 50); //see? It worked!
}

/* I think this tests all the cases. 
 * The problem is I don't know of a good way to test this out other than to 
 * have it be the remove case.
 */
void Test_iterator::test_PostOrder_iterator()
{
  int testArray1[12] = {50, 25, 15, 20, 17, 35, 30, 75, 60, 95, 100, 99};
  int testArray2[12] = {17, 20, 15, 30, 35, 25, 60, 99, 100, 95, 75, 50};
  
  Bst <int> a;  
  BT <int> :: PostOrder z;
  
  for(int i = 0; i < 12; i++)
  {
    a.insert(testArray1[i]);
  }
  z = a.post_begin();
  for(int i = 0; i < 12; i++)
  {
    CPPUNIT_ASSERT(*z == testArray2[i]);
    z++;
  }
}

void Test_iterator::test_PostOrder_dereference()
{
  Bst <int> b;
  
  BT <int> :: PostOrder y;
  
  b.insert(50);
  
  y=b.post_begin();
  CPPUNIT_ASSERT(*y == 50);
}

void Test_iterator::test_PostOrder_equal()
{
  Bst <int> a;
  
  BT <int> :: PostOrder y;
  BT <int> :: PostOrder z;
  
  a.insert(50);
  a.insert(25);
  
  y = a.post_begin();
  z = a.post_begin();
  
  CPPUNIT_ASSERT(y == z);
  z++;
  CPPUNIT_ASSERT(!(y == z));
}

void Test_iterator::test_PostOrder_not_equal()
{
  Bst <int> a;
  
  BT <int> :: PostOrder y;
  BT <int> :: PostOrder z;
  
  a.insert(50);
  a.insert(25);
  
  y = a.post_begin();
  z = a.post_begin();
  
  CPPUNIT_ASSERT(!(y != z));
  z++;
  CPPUNIT_ASSERT(y != z);
}

void Test_iterator::test_PostOrder_search()
{
  int testArray1[12] = {50, 25, 15, 20, 17, 35, 30, 75, 60, 95, 100, 99};  
  
  Bst <int> a;
  BT <int> :: PostOrder z;

  try
  {
    z = a.post_search(6);//Tree does not exist, throw exception
    CPPUNIT_ASSERT(false);
  }
  catch(Exception &e)
  {
    CPPUNIT_ASSERT(e.error_code() == CONTAINER_EMPTY);
  }
  
  for(int i = 0; i < 12; i++)
  {
    a.insert(testArray1[i]);
  }
  
  z = a.post_search(-1);//not in tree
  CPPUNIT_ASSERT(z == NULL);
  
  z = a.post_search(17);//exists
  CPPUNIT_ASSERT(*z == 17);
}

