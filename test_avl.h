//////////////////////////////////////////////////////////////////////////////
/// @file test_avl.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the header file for the test of the avl tree.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @class test_avl
/// @brief This is the header file for the class that tests the avl tree.
//////////////////////////////////////////////////////////////////////////////

#ifndef TEST_AVL_H
#define TEST_AVL_H

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/config/SourcePrefix.h>

#include "bt.h"
#include "bst.h"
#include "avl.h"

class Test_avl : public CPPUNIT_NS::TestFixture
{
  private:
  CPPUNIT_TEST_SUITE (Test_avl);
     CPPUNIT_TEST (test_constructor);
     CPPUNIT_TEST (test_insert_bst);
     CPPUNIT_TEST (test_insert_negneg_root);
     CPPUNIT_TEST (test_insert_negneg_nonroot);
     CPPUNIT_TEST (test_insert_negpos_root);
     CPPUNIT_TEST (test_insert_negpos_nonroot);
     CPPUNIT_TEST (test_insert_posneg_root);
     CPPUNIT_TEST (test_insert_posneg_nonroot);
     CPPUNIT_TEST (test_insert_pospos_root);
     CPPUNIT_TEST (test_insert_pospos_nonroot);

     CPPUNIT_TEST (test_remove_bst);
     CPPUNIT_TEST (test_remove_negneg_root);
     CPPUNIT_TEST (test_remove_negneg_nonroot);
     CPPUNIT_TEST (test_remove_negpos_root);
     CPPUNIT_TEST (test_remove_negpos_nonroot);
     CPPUNIT_TEST (test_remove_posneg_root);
  CPPUNIT_TEST_SUITE_END ();

  protected:
  void test_constructor();

  void test_insert_bst();
  void test_insert_negneg_root();
  void test_insert_negneg_nonroot();
  void test_insert_negpos_root();
  void test_insert_negpos_nonroot();
  void test_insert_posneg_root();
  void test_insert_posneg_nonroot();
  void test_insert_pospos_root();
  void test_insert_pospos_nonroot();  

  void test_remove_bst();
  void test_remove_negneg_root();
  void test_remove_negneg_nonroot();
  void test_remove_negpos_root();
  void test_remove_negpos_nonroot();
    void test_remove_posneg_root();
};

#endif

