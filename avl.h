#ifndef AVL_H
#define AVL_H

#include "bst.h"
template <class generic>
class Avl : public Bst<generic>
{
  public:
    void insert(generic x);
    void remove(generic x);
 
  protected:
    long getDepth(Btn <generic> *);
    int getBalance(Btn <generic> *);
    bool balanced(Btn <generic> *);
    bool unbalanced(Btn <generic> *);
    void setDepth(Btn <generic> *);
    void rotate(Btn <generic> *);
    void rotateNegPos(Btn <generic> *);
    void rotateNegNeg(Btn <generic> *);
    void rotatePosNeg(Btn <generic> *);
    void rotatePosPos(Btn <generic> *);

  private:
    long depth;
};

#include "avl.hpp"
#endif
